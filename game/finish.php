<?php
if (array_key_exists('abandon', $_GET) && $_GET['abandon'] == 1){
    session_unset();
    header('Location: '.$_SERVER['PHP_SELF']);
}



require_once("../components/header.php");
?>
<div class="box">
    <h2>Fini !</h2>
    <?php if ($game['victoire'] == 1) { ?>
        <p>Le mot n'as pas été trouvé</p>
        <div class="pendu status5"></div>
    <?php }  else  { ?>
        <p>Le mot a été trouvé, <?=$game['nom_joueur2']?> est sauvé.e</p>
    <?php } ?>
    <a class="button" href="?abandon=1">Ok</a>
</div>
<?php
require_once("../components/footer.php");
?>
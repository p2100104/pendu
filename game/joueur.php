<?php
if ($_SERVER['REQUEST_METHOD'] == "POST"){
    if (array_key_exists('j1', $_POST) && array_key_exists('j2', $_POST)){
        $res = $dao->insertRow("INSERT INTO partie (nom_joueur1, nom_joueur2, nb_coup) VALUES (?, ?, 1)", [htmlspecialchars($_POST['j1']), htmlspecialchars($_POST['j2'])]);
        if ($res){
            $_SESSION['GAME_ID'] = $res;
            header('Location: '.$_SERVER['PHP_SELF']);
        } else {
            $err = "Il y a eu une erreur dans la création de la partie";
        }

    } else {
        $err = "Il manque le nom d'un joueur";
    }

}

require_once("../components/header.php");
?>

<form method="POST" action="" class="box">
    <?php if (isset($err) &&  $err){ ?>
        <div class="error">
            <p><?=$err?></p>
        </div>
    <?php } ?>
    <div>
        <label for="j1">Joueur 1</label>
        <input type="text" name="j1" id="j1"/>
    </div>
    <div>
        <label for="j2">Joueur 2</label>
        <input type="text" name="j2" id="j2"/>
    </div>
    <div>
        <input class="button" type="submit" value="Commencez"/>
        <a class="button" href="/index.php">Revenir au menu</a>
    </div>
</form>


<?php
require_once("../components/footer.php");
?>
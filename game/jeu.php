<?php
if (array_key_exists('abandon', $_GET) && $_GET['abandon'] == 1){
    $res = $dao->execQuery("UPDATE partie SET victoire = 1 WHERE id = ?", [$_SESSION['GAME_ID']]);
    session_unset();
    header('Location: '.$_SERVER['PHP_SELF']);
}

if (!array_key_exists('findtab', $_SESSION)){
    $_SESSION['findtab'] = [];
}

if ($_SERVER['REQUEST_METHOD'] == "POST"){
   
    $pass = true;
    $newfindtab = [];
    $find = true;
    for ($i = 0; $i < strlen($game['mot']) && $pass; $i++){
        if (array_key_exists("l{$i}", $_POST) && $_POST["l{$i}"]){
            if (strtolower($_POST["l{$i}"]) == $game['mot'][$i]){
                $newfindtab[] = $game['mot'][$i];
            } else {
                $find = false;
                $newfindtab[] = $_SESSION['findtab'][$i];
            }
        } else {
            $pass = false;
        }
    }
    $_SESSION['findtab'] = $newfindtab;

    if ($pass){
        if ($find){
            //win
            $res = $dao->execQuery("UPDATE partie SET victoire = 2 WHERE id = ?", [$_SESSION['GAME_ID']]);
            if ($res > 0){
                header('Location: '.$_SERVER['REQUEST_URI']);
            } else {
                $err = "Erreur dans l'enregistrement de la réponse";
            }
        } else {
            if ($game['nb_coup'] < 5){
                $res = $dao->execQuery("UPDATE partie SET nb_coup = nb_coup+1 WHERE id = ?", [$_SESSION['GAME_ID']]);
                if ($res <= 0){
                    $err = "Erreur dans l'enregistrement de la réponse";
                } else {
                    $game['nb_coup']++;
                    $err = "Raté !";
                }
            } else {
                //loose
                $res = $dao->execQuery("UPDATE partie SET victoire = 1 WHERE id = ?", [$_SESSION['GAME_ID']]);
                if ($res > 0){
                    header('Location: '.$_SERVER['REQUEST_URI']);
                } else {
                    $err = "Erreur dans l'enregistrement de la réponse";
                }
            }
        }
    } else {
        $err = "Il manque le mot";
    }

}

require_once("../components/header.php");
?>

<form method="POST" class="box">
    <h2>Joueur : <?=$game['nom_joueur2']?></h2>
    <?php if (isset($err) &&  $err){ ?>
        <div class="error">
            <p><?=$err?></p>
        </div>
    <?php } ?>
    <p>C'est un mot en <?=strlen($game['mot'])?> lettres</p>
    <div>
        <?php for ($i = 0; $i < strlen($game['mot']); $i++){
            echo "<input required type='text' class='mot' name='l{$i}' id='l{$i}' maxlength=1 value='{$_SESSION['findtab'][$i]}'/>";
        } ?>
    </div>
    <p>Nombre d'essai : <?=$game['nb_coup']?>/5</p>
    <div class="pendu status<?=$game['nb_coup']?>"></div>
    <div>
        <input class="button" type="submit" value="Essayer !">
        <a class="button" href="?abandon=1">Abandonner</a>
    </div>
</form>

<script>
    (() => {
        var cases = document.getElementsByClassName("mot");
        for (var i = 0; i < cases.length-1; i++){
            cases[i].addEventListener("input", (e) => {
                e.target.nextSibling.focus();
            })
        }
    })()
</script>

<?php
require_once("../components/footer.php");
?>
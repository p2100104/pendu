<?php

if (array_key_exists('abandon', $_GET) && $_GET['abandon'] == 1){
    $res = $dao->execQuery("DELETE FROM partie WHERE id = ?", [$_SESSION['GAME_ID']]);
    session_unset();
    header('Location: '.$_SERVER['PHP_SELF']);
}

if ($_SERVER['REQUEST_METHOD'] == "POST"){
    if (array_key_exists('mot', $_POST) && $_POST['mot']){
        $res = $dao->execQuery("UPDATE partie SET mot = ? WHERE id = ?", [strtolower(htmlspecialchars($_POST['mot'])), $_SESSION['GAME_ID']]);
        if ($res > 0){
            header('Location: '.$_SERVER['PHP_SELF']);
        } else {
            $err = "Une erreur dans l'ajout du mot";
        }
    } else {
        $err = "Il manque le mot";
    }

}

require_once("../components/header.php");
?>

<form method="POST" action="" class="box">
    <h2>Joueur : <?=$game['nom_joueur1']?></h2>
    <?php if (isset($err) &&  $err){ ?>
        <div class="error">
            <p><?=$err?></p>
        </div>
    <?php } ?>
    <div>
        <label for="mot">Choisissez votre mot :</label>
        <input type="text" name="mot" id="mot" placeholder="abracadabra"/>
    </div>
    <div>
        <input class="button" type="submit" value="Valider !">
        <a class="button" href="?abandon=1">Abandonner</a>
    </div>
</form>

<?php
require_once("../components/footer.php");
?>
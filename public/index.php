<?php
session_start();

require_once("../components/header.php");

require_once("../database/DAO.php");
$dao = new Dao(false);
$list = $dao->queryAll("SELECT * FROM partie", []);
?>

<header>
    <h1>Jeu du pendu</h1>
    <a class="button" href="/jouer.php">Jouer</a>
</header>

<main>
    <h2>Liste des parties</h2>
    <table>
        <thead>
            <th>Joueur 1</th>
            <th>Joueur 2</th>
            <th>Mot</th>
            <th>Nombre de coups</th>
        </thead>
        <tbody>
            <?php foreach($list as $line) {?>
                <tr class="winner<?=$line['victoire']?>">
                    <td><?=$line['nom_joueur1']?></td>
                    <td><?=$line['nom_joueur2']?></td>
                    <td><?=$line['mot']?></td>
                    <td><?=$line['nb_coup']?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</main>

<?php
require_once("../components/footer.php");
?>
<?php
session_start();

require_once("../database/DAO.php");
$dao = new Dao(false);

if (array_key_exists('GAME_ID', $_SESSION)){
    $game = $dao->queryRow("SELECT * FROM partie WHERE id = ?", [$_SESSION['GAME_ID']]);  
    if ($game){
        if (!$game['mot']){
            require_once("../game/def_mot.php");
        } else if (!$game['victoire']){
            require_once("../game/jeu.php");
        } else {
            require_once("../game/finish.php");
        }
    } else {
        //Erreur quelque part : Reinitialisation de la session + refresh
        session_unset();
        header('Location: '.$_SERVER['REQUEST_URI']);
    }

} else {
    require_once("../game/joueur.php");
}